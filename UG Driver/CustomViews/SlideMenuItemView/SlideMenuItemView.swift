//
//  SlideMenuItemView.swift
//  Welltravel
//
//  Created by Amit Sen on 10/1/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit
import Bond

class SlideMenuItemView: UIView {
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    let viewModel = SlideMenuItemViewModel.init()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    // private methods
    private func customInit() {
        Bundle.main.loadNibNamed("SlideMenuItemView", owner: self, options: nil)
//        containerView.backgroundColor = viewModel.colors.buttonBg
//        nameLabel.textColor = viewModel.colors.buttonText
        self.addSubview(containerView)
        containerView.frame = self.bounds
    }
    
    // public methods
    public func setData(icon image: UIImage, title name: String, onBtnPressed callback: @escaping(UIButton) -> Void) {
        self.iconView.image = image
        self.nameLabel.text = name
        _ = btn.reactive.tap.observeNext {
            callback(self.btn)
        }
    }
}
