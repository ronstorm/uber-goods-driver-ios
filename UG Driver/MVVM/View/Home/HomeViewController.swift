//
//  HomeViewController.swift
//  UG Customer
//
//  Created by Amit Sen on 4/30/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    // IBOutlets
    @IBOutlet weak var requestBtn: UIButton!
    
    // public variables
    
    // private variables
    var viewModel: HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        placeCornerBtn(withCornerBtn: .menu)
        viewModel = HomeViewModel.init()
        setViews()
    }
    
    // IBActions
    @IBAction func requestBtnPressed(_ sender: UIButton) {
    }
    
    // private methods
    private func setViews() {
        requestBtn.backgroundColor = .white
        requestBtn.layer.backgroundColor = viewModel.colors.go_btn.cgColor
        requestBtn.makeCircular(borderWidth: 0.0, borderColor: .clear)
    }
}
