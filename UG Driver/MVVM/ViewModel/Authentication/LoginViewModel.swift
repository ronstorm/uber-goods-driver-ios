//
//  LoginViewModel.swift
//  UG Customer
//
//  Created by Amit Sen on 3/8/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import UIKit

class LoginViewModel: BaseViewModel {

    override init() {
        super.init()
    }
    
    func doLogin(onComplete callback: @escaping(UserData) -> Void) {
        
        networkManager.apiCallForObjectResponse(endpointURL: apiPaths.login,
                                                httpMethod: .post,
                                                parameters: [:],
                                                isMultiPart: false,
                                                filesWhenMultipart: nil,
                                                returningType: User.self) { (user, jsonData) in
                                                    print("JSON: \(jsonData)")
                                                    
                                                    if user.statusCode == 200 {
                                                        callback(user.userData)
                                                    } else {
                                                        self.errorManager.errorMessage.next((self.texts.error, self.texts.dataNotFound))
                                                    }
                                                    
        }
    }
}
